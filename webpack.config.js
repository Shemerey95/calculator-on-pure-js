const path                 = require('path');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const autoPrefixer         = require('autoprefixer');

module.exports = {
    entry:   [
        // '@babel/polyfill',
        // 'whatwg-fetch',
        // 'element-closest-polyfill',
        // 'element-remove-polyfill',
        './src/app.js'
    ],
    output:  {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module:  {
        rules: [
            {
                // test: /\.css|scss$/,
                // use:  [
                //     'style-loader',
                //     MiniCssExtractPlugin.loader,
                //     {
                //         loader:  'css-loader',
                //         options: {sourceMap: true}
                //     }, {
                //         loader: 'postcss-loader',
                //         options: {
                //             plugins: [
                //                 autoPrefixer({
                //                     overrideBrowserslist:['ie >= 8', 'last 4 version']
                //                 })
                //             ],
                //             sourceMap: true
                //         }
                //     }, {
                //         loader:  'sass-loader',
                //         options: {sourceMap: true}
                //     }
                // ]
            }, {
                // test:    /\.m?js$/,
                // exclude: /(node_modules|bower_components)/,
                // use:     {
                //     loader:  'babel-loader',
                //     options: {
                //         presets: ['@babel/preset-env'],
                //         plugins: ['@babel/plugin-proposal-object-rest-spread']
                //     }
                // }
            }
        ]
    },
    plugins: [
        // new MiniCssExtractPlugin({
        //     filename: "[name].css"
        // })
    ]
};