"use strict";

document.addEventListener('DOMContentLoaded', () => {

    const beginningCalculationButton          = document.querySelector('.start-button');
    const firstScreen                         = document.querySelector('.first-screen');
    const mainForm                            = document.querySelector('.main-form');
    const cardHead                            = document.querySelector('.card-head');
    const formCalculate                       = document.querySelector('.form-calculate');
    const formCalculateEndButton              = formCalculate.querySelector('.end-button');
    const formCalculateTotal                  = formCalculate.querySelector('.total');
    const formFastRange                       = formCalculate.querySelector('.fast-range');
    const formTotalPrice                      = formCalculate.querySelector('.total_price');
    const formFinalTotalPrice                 = formCalculate.querySelector('.total_price__sum');
    const formSwitchMobileDeveloper           = document.getElementById('mobileTemplates');
    const formSiteDevelopmentTimeTextTypeSite = formCalculate.querySelector('.type-site');
    const formMaxDeadlineFree                 = formCalculate.querySelector('.max-deadline');
    const formRangeDeadline                   = formCalculate.querySelector('.range-deadline');
    const formDeadlineValue                   = formCalculate.querySelector('.deadline-value');
    const formCheckboxLabel                   = formCalculate.querySelectorAll('.checkbox-label');
    const formFastRangeValueText              = formCalculate.querySelector('.fastRange_value');
    const formSubtotalCalcDescription         = formCalculate.querySelector('.calc-description');


    // основной объект для расчета итоговой цены
    const DATA = {
        whichSite:        ['landing', 'multiPage', 'onlineStore'],
        price:            [4000, 8000, 26000],
        desktopTemplates: [50, 40, 30],
        adapt:            20,
        mobileTemplates:  15,
        editable:         10,
        metrikaYandex:    [500, 1000, 2000],
        analyticsGoogle:  [850, 1350, 3000],
        sendOrder:        500,
        deadline:         [[2, 7], [3, 10], [7, 14]],
        deadlinePercent:  [20, 17, 15]
    };

    const daysString = ['день', 'дня', 'дней'];

    // склонение времени
    const declOfNum = (n, titles, from) => n + ' ' + titles[from ? n % 10 === 1 && n % 100 !== 11 ?
        1 : 2 : n % 10 === 1 && n % 100 !== 11 ?
        0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];

    // рендер изменяющегося текста на странице
    const renderTextContent = (optionsData, totalPrice, typeSite, maxDay, minDay) => {
        formFinalTotalPrice.textContent                 = totalPrice.toString();
        formSiteDevelopmentTimeTextTypeSite.textContent = typeSite;
        formMaxDeadlineFree.textContent                 = declOfNum(maxDay, daysString, true);
        formRangeDeadline.min                           = minDay;
        formRangeDeadline.max                           = maxDay;
        formRangeDeadline.defaultValue                  = maxDay;

        formCheckboxLabel.forEach(item => {
            if (!item.classList.contains('fastRange_value')) {
                item.textContent = optionsData.includes(item.dataset.checkbox) ? 'Да' : 'Нет';
            }
        });

        formFastRangeValueText.textContent = optionsData.includes('fastDevelopment') ? 'Хочу быстрее!' : 'Хочу быстрее';
        formDeadlineValue.textContent      = declOfNum(formRangeDeadline.value, daysString);

        renderSubtotalDescription(typeSite, optionsData.includes('adapt'),
            optionsData.includes('metrikaYandex'), optionsData.includes('analyticsGoogle'),
            optionsData.includes('sendOrder'), optionsData.includes('editable'));
    };

    // генерация содержания выбранных работ по разработке сайта
    const renderSubtotalDescription = (typeSite, adapt, metrikaYandex, analyticsGoogle, sendOrder, editable) => {
        const descriptionAdapt        = adapt ? ', адаптированный под мобильные устройства и планшеты' : '';
        const descriptionAdditionally = metrikaYandex || analyticsGoogle || sendOrder ?
            `Подключим ${metrikaYandex ?
                'Яндекс Метрику' : ''}${analyticsGoogle ?
                metrikaYandex ?
                    ', Гугл Аналитику' : 'Гугл Аналитику' : ''}${sendOrder ?
                metrikaYandex || analyticsGoogle ?
                    ' и отправку заявок на почту' : 'отправку заявок на почту' : ''}.` : '';

        formSubtotalCalcDescription.textContent = `Сделаем ${typeSite}${descriptionAdapt}.
        ${editable ? 'Установим панель админстратора, чтобы вы могли самостоятельно менять содержание на сайте без разработчика.' : ''}
        ${descriptionAdditionally}`;
    };

    // показ блока
    const showItem = item => item.style.display = 'block';

    // сокрытие блока
    const disabledItem = item => item.style.display = 'none';

    // расчет итоговой цены
    const priceFormCalculation = (item = {}) => {
        const {
                  whichSite,
                  price,
                  desktopTemplates,
                  deadline,
                  deadlinePercent
              } = DATA;


        let totalPrice     = 0;
        let indexWhichSite = 0;
        let typeSite       = 'одностраничный сайт';
        let maxDeadline    = deadline[indexWhichSite][1];
        let minDeadline    = deadline[indexWhichSite][0];
        const optionsData  = [];

        if (item.name === 'whichSite') {
            for (const elem of formCalculate.elements) {
                if (elem.type === 'checkbox') {
                    elem.checked = false;
                }
            }
            disabledItem(formFastRange);
        }

        for (const elem of formCalculate.elements) {
            if (elem.name === 'whichSite' && elem.checked) {
                indexWhichSite = whichSite.indexOf(elem.value);
                typeSite       = elem.dataset.site;
                maxDeadline    = deadline[indexWhichSite][1];
                minDeadline    = deadline[indexWhichSite][0];
            } else if (elem.checked && elem.name !== 'deadline') {
                optionsData.push(elem.value);
            }
        }

        optionsData.forEach(key => {
            if (typeof DATA[key] === 'number') {
                if (key === 'sendOrder') {
                    totalPrice += DATA[key];
                } else {
                    totalPrice += price[indexWhichSite] * DATA[key] / 100;
                }
            } else {
                if (key === 'desktopTemplates') {
                    totalPrice += price[indexWhichSite] * desktopTemplates[indexWhichSite] / 100;
                } else if (key === 'fastDevelopment') {
                    totalPrice += price[indexWhichSite] * deadlinePercent[indexWhichSite] / 100;
                } else {
                    totalPrice += DATA[key][indexWhichSite];
                }
            }
        });

        totalPrice += DATA.price[indexWhichSite];

        renderTextContent(optionsData, totalPrice, typeSite, maxDeadline, minDeadline);
    };

    // отслежтвание изменения в форме
    const handlerForm = event => {
        const target = event.target;

        if (target.id === 'adapt') {
            if (target.checked) {
                formSwitchMobileDeveloper.disabled = false;
            } else {
                formSwitchMobileDeveloper.checked  = false;
                formSwitchMobileDeveloper.disabled = true;
            }
        }

        if (target.classList.contains('want-faster')) target.checked ?
            showItem(formFastRange) : disabledItem(formFastRange);

        if (target.classList.contains('calc-handler')) priceFormCalculation(target);
    };

    // успешная отправка данных на сервер
    const renderResponse = response => {
        if (response.ok) {
            disabledItem(formCalculateTotal);
            disabledItem(formTotalPrice);
            cardHead.textContent = 'Ваша заявка была отправлена, мы с Вами свяжемся в ближайшее время. Спасибо!';
            cardHead.style.fontSize = '2.5rem';
            cardHead.style.color = 'green';

            setTimeout(() => {
                document.location.href = "/";
            }, 2500);
        }
    };

    // отправка данных на сервер
    const formSubmit = event => {
        event.preventDefault();

        const formData = new FormData(event.target);

        fetch('./handler.php', {
            method:  'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body:    formData
        })
        .then(renderResponse)
        .catch(error => {
                console.error(error);
            }
        )
    };

//обработчики событий
    beginningCalculationButton.addEventListener('click', event => {
        event.preventDefault();

        showItem(mainForm);
        disabledItem(firstScreen);
    });

    formCalculateEndButton.addEventListener('click', () => {
        // formCalculate.querySelectorAll('fieldset').forEach(item => disabledItem(item));
        for (const item of formCalculate.elements) {
            item.tagName === 'FIELDSET' ? disabledItem(item) : '';
        }
        showItem(formCalculateTotal);
    });

    formCalculate.addEventListener('change', handlerForm);
    formCalculate.addEventListener('submit', formSubmit);
});
